#!/usr/bin/env bash

# Web Page of BASH best practices https://kvz.io/blog/2013/11/21/bash-best-practices/
#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename "${__file}" .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

echo "Script name:${__base}"
echo "Executing at ${__root}"

GITLAB_VERSION="0.3.0"
GITLAB_EXECUTE="gitlab-ci-validate"

HOST_TYPE="$(uname -s)"
if [ "${HOST_TYPE}" == "Darwin" ]; then
    HOST_TYPE="darwin_amd64"
else
    HOST_TYPE="linux_amd64"
fi

if ! type "$GITLAB_EXECUTE" &> /dev/null; then
    GITLAB_EXECUTE="binaries/${GITLAB_EXECUTE}"
    if ! type "$GITLAB_EXECUTE" &> /dev/null; then
        if ! type "curl" &> /dev/null; then
            echo "curl is not installed. Install it and then re launch"
            exit 1
        fi
        mkdir -p binaries/gitlab
        DOWNLOAD_URL="https://github.com/Code0x58/gitlab-ci-validate/releases/download/v${GITLAB_VERSION}/gitlab-ci-validate_${GITLAB_VERSION}_${HOST_TYPE}.tar.gz"
        echo "Dowloading binary from ${DOWNLOAD_URL}"
        curl -L "${DOWNLOAD_URL}" -o binaries/gitlab.tar.gz

        tar -xvf binaries/gitlab.tar.gz -C binaries/gitlab
        cp binaries/gitlab/gitlab-ci-validate binaries/gitlab-ci-validate
        rm -rf binaries/gitlab
        rm binaries/gitlab.tar.gz
        chmod +x binaries/gitlab-ci-validate
    fi
fi

mkdir -p binaries

if [ ! -f "binaries/common_test.sh" ]; then
    curl -o binaries/common_test.sh -L https://singletonsd.gitlab.io/scripts/common/latest/bash_common.sh
    chmod +x binaries/common_test.sh
fi

HOST_TYPE="$(uname -s)"
if [ "${HOST_TYPE}" == "Darwin" ]; then
    echo "WARN: can't execute gitlab lint on macOS systems"
    exit
fi

eval "binaries/common_test.sh" -b="$GITLAB_EXECUTE" "$@"